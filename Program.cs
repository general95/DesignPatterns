﻿using System;
using System.Collections.Generic;
using DesignPatterns.Patterns;

namespace DesignPatterns
{
    class Program
    {
        static readonly Dictionary<int, (string patternName, IPattern patternClass)> _designPatternsList =
                new Dictionary<int, (string, IPattern)>() {
                    { 1, ("Strategy", new Patterns.StrategyPattern.Pattern() ) },
                    { 2, ("Simple Factory", new Patterns.SimpleFactoryPattern.Pattern()) },
                    { 3, ("Factory Method", new Patterns.FactoryMethodPattern.Pattern()) },
                    { 4, ("Abstract Factory", new Patterns.AbstractFactoryPattern.Pattern()) }
                };
        static void Main(string[] args)
        {
            Console.WriteLine("Design Paterns:");
            Console.WriteLine("Number | Name");
            foreach (var item in _designPatternsList)
            {
                Console.WriteLine(item.Key + " | " + item.Value.patternName);
            }

            Console.WriteLine("Please enter the patterns number: ");

            int pNumber;
            while (!int.TryParse(Console.ReadLine(), out pNumber) || !_designPatternsList.ContainsKey(pNumber))
            {
                Console.WriteLine("The input was not valid! please try again: ");
            };

            _designPatternsList[pNumber].patternClass.Run();
        }

    }
}
