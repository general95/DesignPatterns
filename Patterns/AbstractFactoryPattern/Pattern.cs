using System;

namespace DesignPatterns.Patterns.AbstractFactoryPattern
{
    public class Pattern : IPattern
    {
        public void Run()
        {
            Console.WriteLine("AbstractFactoryPattern Run Method");
        }
    }
}