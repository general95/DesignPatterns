using System;

namespace DesignPatterns.Patterns.SimpleFactoryPattern
{
    public class Pattern : IPattern
    {
        public void Run()
        {
            Console.WriteLine("SimpleFactoryPattern Run Method");
        }
    }
}