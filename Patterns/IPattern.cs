namespace DesignPatterns.Patterns
{
    interface IPattern
    {
        void Run();
    }
}