using System;

namespace DesignPatterns.Patterns.FactoryMethodPattern
{
    public class Pattern : IPattern
    {
        public void Run()
        {
            Console.WriteLine("FactoryMethodPattern Run Method");
        }
    }
}