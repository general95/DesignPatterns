namespace DesignPatterns.Patterns.StrategyPattern.Example1
{
    interface IBillingStrategy
    {
        double GetActPrice(double rawPrice);
    }
}