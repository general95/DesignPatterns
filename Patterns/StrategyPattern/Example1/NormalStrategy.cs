namespace DesignPatterns.Patterns.StrategyPattern.Example1
{
    // Normal billing strategy (unchanged price)
    class NormalStrategy : IBillingStrategy
    {
        public double GetActPrice(double rawPrice)
        {
            return rawPrice;
        }
    }
}