namespace DesignPatterns.Patterns.StrategyPattern.Example1
{
    // Strategy for Happy hour (50% discount)
    class HappyHourStrategy : IBillingStrategy
    {
        public double GetActPrice(double rawPrice)
        {
            return rawPrice * 0.5;
        }
    }
}