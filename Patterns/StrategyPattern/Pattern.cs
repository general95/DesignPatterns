using System;
using DesignPatterns.Patterns.StrategyPattern.Example1;

namespace DesignPatterns.Patterns.StrategyPattern
{
    public class Pattern : IPattern
    {
        public void Run()
        {
            Console.WriteLine("StrategyPattern Run Method");
            var strategyPatternExample1 = new StrategyPatternExample1();
            strategyPatternExample1.RunPattern();
        }
    }
}